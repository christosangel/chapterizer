#! /bin/bash
#
#  ▄▄ ▗▖                              █
# █▀▀▌▐▌              ▐▌              ▀
#▐▛   ▐▙██▖ ▟██▖▐▙█▙ ▐███  ▟█▙  █▟█▌ ██  ▐███▌ ▟█▙  █▟█▌
#▐▌   ▐▛ ▐▌ ▘▄▟▌▐▛ ▜▌ ▐▌  ▐▙▄▟▌ █▘    █    ▗▛ ▐▙▄▟▌ █▘
#▐▙   ▐▌ ▐▌▗█▀▜▌▐▌ ▐▌ ▐▌  ▐▛▀▀▘ █     █   ▗▛  ▐▛▀▀▘ █
# █▄▄▌▐▌ ▐▌▐▙▄█▌▐█▄█▘ ▐▙▄ ▝█▄▄▌ █   ▗▄█▄▖▗█▄▄▖▝█▄▄▌ █
#  ▀▀ ▝▘ ▝▘ ▀▀▝▘▐▌▀▘   ▀▀  ▝▀▀  ▀   ▝▀▀▀▘▝▀▀▀▘ ▝▀▀  ▀
#               ▐▌
#  A simple bash script to split txt files to chapters.
#  Written by Christos Angelopoulos, April 2022
FILE="$(yad --file-selection --filename=Desktop --height='400' --width='800' --title='Chapterizer'  --window-icon=/usr/share/icons/Mint-Y/apps/96@2x/calibre.png --hscroll-policy=never --vscroll-policy=never)"
case $? in
  0)
  ;;
  1) exit
  ;;
esac
DIRECTORY=${FILE%/*}/
NAME=${FILE##*/}
echo "FILE : ""$FILE"
echo "DIRECTORY : " "$DIRECTORY"
echo "NAME : ""$NAME"
#USEFUL COMMANDS TO USE:
#sed -i '/^.[0-9]*$/ s/^/Chapter /' XXX.txt
#sed '/^Chapter 1$/ s/^/PART \n\n/' XXX.txt
CHAPTER="$(yad --image=/usr/share/icons/Mint-Y/apps/96@2x/calibre.png --entry --width="400" --text="Enter the appropriate word for delimiting the text to chapters(e.g. Chapter, Part etc)"  --window-icon=/usr/share/icons/Mint-Y/apps/96@2x/calibre.png --title="Chapterizer")"
case $? in
  0)
  ;;
  1) exit
  ;;
esac
grep -n "$CHAPTER" "$FILE" |sed 's/\:/ /g'|awk '{print $1}'>"$DIRECTORY"delim_lines.txt
TEXTLENGTH=$(cat "$FILE"|wc -l)
echo $TEXTLENGTH>>"$DIRECTORY"delim_lines.txt
TOTALLINES=$(cat "$DIRECTORY"delim_lines.txt|wc -l)

LINE1=1
while [ $LINE1 -lt $TOTALLINES ]
do
 LINE2=$(($LINE1 + 1))
 FIRST=$(cat "$DIRECTORY"delim_lines.txt|head -$LINE1|tail +$LINE1)
 LAST=$(cat "$DIRECTORY"delim_lines.txt|head -$LINE2|tail +$LINE2)
 ((LAST--))
	LENGTH=$(($LAST - FIRST +1)) #+1


 cat "$FILE"|head -$LAST|tail -$LENGTH>"$DIRECTORY""$CHAPTER"$LINE1".txt";
 #sed -i '$d' "$DIRECTORY""$CHAPTER"$LINE1".txt" #removes last line
 ((LINE1++))
done
yad --image=/usr/share/icons/Mint-Y/apps/96@2x/calibre.png  --width="600" --text="Chapterizing of

<span foreground='yellow'><b>$NAME</b></span>

was succesful."  --window-icon=/usr/share/icons/Mint-Y/apps/96@2x/calibre.png --title="Chapterizer"
